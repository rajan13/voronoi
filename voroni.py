# -*- coding: utf-8 -*-
"""
Created on Tue Aug 01 21:32:23 2017

@author: Razhan13
"""

import cv2
import numpy as np
import random


def draw_voronoi(img, subdiv) :
 
    ( facets, centers) = subdiv.getVoronoiFacetList([])
 
    for i in xrange(0,len(facets)) :
        ifacet_arr = []
        for f in facets[i] :
            ifacet_arr.append(f)
         
        ifacet = np.array(ifacet_arr, np.int)
        color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
 
        cv2.fillConvexPoly(img, ifacet, color, cv2.LINE_AA, 0);
        ifacets = np.array([ifacet])
        cv2.polylines(img, ifacets, True, (0, 0, 0), 1, cv2.LINE_AA, 0)
        cv2.circle(img, (centers[i][0], centers[i][1]), 3, (0, 0, 0) , cv2.LINE_AA, 0)
        

def rect_contains(rect, point) :
    if point[0] < rect[0] :
        return False
    elif point[1] < rect[1] :
        return False
    elif point[0] > rect[2] :
        return False
    elif point[1] > rect[3] :   
        return False
    return True

if __name__ == '__main__':
    points = [(200,600),(400, 800), (600, 600), (800, 800)]
    #points = np.array([[200, 600], [400,800], [600, 600], [800,800]])
    
    
    img = cv2.imread('IMG.png')
    size = img.shape
    rect = (0, 0, size[1], size[0])
    print rect
    
    subdiv = cv2.Subdiv2D(rect)
    print subdiv
    
    for p in points :
        subdiv.insert(p)
    
    
    img_voronoi = np.zeros(img.shape, dtype = img.dtype)
    
    draw_voronoi(img_voronoi, subdiv)
    
    cv2.imshow('Voronoi Diagram',img_voronoi)
    cv2.waitKey(0)